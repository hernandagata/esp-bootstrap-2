import {v4 as uuid} from 'uuid';

export class DestinoViaje {
	selected: boolean;
	servicios: string[];
	id = uuid();

	constructor(public nombre:string, public url:string, public votes: number = 0){
		this.servicios = ['Desayuno', 'Piscina', 'Cena', 'Barra libre'];
	}

	isSelected() {
		return this.selected;
	}

	setSelected(s: boolean) {
		this.selected = s;
	}

	voteUp() {
		this.votes++;
	}

	voteDown() {
		this.votes--;
	}
}